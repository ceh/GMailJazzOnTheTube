from __future__ import print_function

import base64
import datetime
import email
import email.policy
import os
import re
import sys
import urllib.request
from typing import (List, Set)

import apiclient
import httplib2
import oauth2client.client
import oauth2client.file
import oauth2client.tools

try:
    # noinspection PyUnresolvedReferences
    import argparse

    flags = argparse.ArgumentParser(parents=[oauth2client.tools.argparser]).parse_args()
except ImportError:
    flags = None

YOUTUBE_SCOPE = 'https://www.googleapis.com/auth/youtube'
GMAIL_SCOPE = 'https://www.googleapis.com/auth/gmail.modify'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Quickstart'

MsgID = str  # gmail msg id
PlaylistID = str  # youtube playlist id
VideoID = str  # youtube video id
URL = str


class Gmail:
    def __init__(self, http):
        self.svc = apiclient.discovery.build('gmail', 'v1', http=http)

    def query(self, _query: str) -> List[MsgID]:
        response = self.svc.users().messages().list(
            userId='me',
            fields='messages(id),nextPageToken',
            q=_query
        ).execute()

        msg_ids = []
        if 'messages' in response:
            msg_ids.extend([msg['id'] for msg in response['messages']])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = self.svc.users().messages().list(
                userId='me',
                fields='messages(id),nextPageToken',
                q=_query,
                pageToken=page_token).execute()
            msg_ids.extend([msg['id'] for msg in response['messages']])

        return msg_ids

    def archive(self, msg_id: MsgID) -> None:
        self.svc.users().messages().modify(userId='me', id=msg_id,
                                           body={'removeLabelIds': ['INBOX'], 'addLabelIds': []}).execute()

    def msg(self, msg_id: MsgID) -> email.message.Message:
        msg = None
        try:
            message = self.svc.users().messages().get(userId='me', format='raw', id=msg_id).execute()
            msg_bytes = base64.urlsafe_b64decode(message['raw'])
            msg = email.message_from_bytes(msg_bytes, policy=email.policy.default)
        except apiclient.errors.HttpError as e:
            print('An error occurred: %s' % e)
        return msg


class Message:
    def __init__(self, message_id: MsgID, gmail: Gmail):
        self.id = message_id
        self.gmail = gmail
        self.msg = self.gmail.msg(self.id)
        self.date = self.msg['date'].datetime
        self.urls = re.findall(r'(https?://[-a-zA-Z0-9@:_\+.~#?&/=%]+)', self.msg.get_payload())

    def archive(self) -> None:
        self.gmail.archive(self.id)


class Youtube:
    def __init__(self, http):
        self.svc = apiclient.discovery.build('youtube', 'v3', http=http)
        response = self.svc.channels().list(
            part='contentDetails',
            fields='items/contentDetails/relatedPlaylists/watchHistory',
            mine=True
        ).execute()
        self.watched = Playlist(
            response['items'][0]['contentDetails']['relatedPlaylists']['watchHistory'],
            'Watched',
            self)
        request = self.svc.playlists().list(
            part='snippet',
            fields='items(id,snippet(title)),nextPageToken',
            mine=True,
            maxResults=50)
        while request:
            response = request.execute()
            for item in response['items']:
                Playlist(item['id'], item['snippet']['title'], self)
            request = self.svc.playlists().list_next(request, response)

    def create_playlist(self, title: str) -> PlaylistID:
        playlists_insert_response = self.svc.playlist_ids().insert(
            part='snippet,status',
            body={
                'snippet': {'title': title, 'description': 'A private playlist for ' + title},
                'status': {'privacyStatus': 'private'}
            }).execute()
        playlist_id = playlists_insert_response['id']
        print(u'created playlist "{0:s}" with id {1:s}'.format(title, playlist_id))
        return playlist_id

    def playlist_video_ids(self, _id: PlaylistID) -> Set[VideoID]:
        video_ids = set()
        request = self.svc.playlistItems().list(
            playlistId=_id,
            part='snippet',
            fields='items/snippet/resourceId/videoId,nextPageToken',
            maxResults=50
        )

        sys.stdout.write('fetching video ids for playlist %s ' % _id)
        sys.stdout.flush()
        while request:
            response = request.execute()
            sys.stdout.write('.')
            sys.stdout.flush()
            video_ids |= {item['snippet']['resourceId']['videoId'] for item in response['items']}
            request = self.svc.playlistItems().list_next(request, response)
        sys.stdout.write('\n')
        sys.stdout.flush()
        return video_ids

    def add_video_to_playlist(self, video_id: VideoID, _id: PlaylistID) -> None:
        body = {
            'snippet': {
                'playlistId': _id,
                'resourceId': {'videoId': video_id, 'kind': 'youtube#video'}}}
        try:
            self.svc.playlistItems().insert(part=','.join(body.keys()), body=body).execute()
        except apiclient.errors.HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
            print('Inserting %s into playlist %s' % (video_id, self))
            raise YouTubeInsertFailed(e)


class DuplicatePlaylist(Exception):
    pass


class Playlist:
    _playlists = {}  # global playlist cache TODO declare the type

    def __init__(self, playlist_id: PlaylistID, title: str, youtube: Youtube):
        if playlist_id in Playlist._playlists:
            raise DuplicatePlaylist(playlist_id)
        self.id = playlist_id
        self.youtube = youtube
        self._video_ids = None
        self.title = title
        Playlist._playlists[playlist_id] = self

    @staticmethod
    def make(playlist_id: PlaylistID, title: str, youtube: Youtube) -> 'Playlist':
        if playlist_id in Playlist._playlists:
            # noinspection PyMethodFirstArgAssignment,PyUnusedLocal
            return Playlist._playlists[playlist_id]
        return Playlist(playlist_id, title, youtube)

    @staticmethod
    def by_date(date: datetime.date, youtube: Youtube) -> 'Playlist':
        title = date.strftime('Jazz on the Tube %b %Y')
        for (key, value) in Playlist._playlists.items():
            if value.title == title:
                return value
        playlist_id = youtube.create_playlist(title)
        return Playlist(playlist_id, title, youtube)

    @property
    def video_ids(self):
        if self._video_ids is None:
            self._video_ids = self.youtube.playlist_video_ids(self.id)
        return self._video_ids

    def add(self, _video_id: VideoID) -> None:
        if _video_id in self.video_ids:
            print('add duplicate %s' % _video_id)
            return
        if _video_id in self.youtube.watched.video_ids:
            print('add watched %s' % _video_id)
            return
        self.youtube.add_video_to_playlist(_video_id, self.id)
        self._video_ids.add(_video_id)
        print('Video %s added to playlist %s' % (_video_id, self.title))

    def __str__(self):
        return self.title


def authenticate(credential_path, store):
    scopes = GMAIL_SCOPE + ' ' + YOUTUBE_SCOPE
    flow = oauth2client.client.flow_from_clientsecrets(CLIENT_SECRET_FILE, scopes)
    flow.user_agent = APPLICATION_NAME
    _credentials = ''
    if flags:
        _credentials = oauth2client.tools.run_flow(flow, store, flags)
    print('Storing credentials to ' + credential_path)
    return _credentials


def credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'gmail-jazzonthetube.json')

    store = oauth2client.file.Storage(credential_path)
    _credentials = store.get()
    if not _credentials or _credentials.invalid:
        _credentials = authenticate(credential_path, store)
    return _credentials


# pattern from http://stackoverflow.com/questions/6903823/regex-for-youtube-id
# noinspection SpellCheckingInspection
youtube_url_pat = r'''
    (?:youtube\.com\/
        (?:[^\/]+\/[^?&]+\/     # youtube.com/xxx/yyy/
        |(?:v|e(?:mbed)?)\/ # youtube.com/v/
                            # youtube.com/e/
                            # youtube.com/embed/
        |.*[?&]v=)          # youtube.com/xxx?v=
                            # youtube.com/xxx?yyy&v=
    |youtu\.be\/)           # youtu.be/
    ([^"&?\/ ]{11})         # the actual ID
    '''
youtube_url_pat_re = re.compile(youtube_url_pat, re.IGNORECASE | re.VERBOSE)


def video_ids_from_url(url: URL) -> Set[VideoID]:
    with urllib.request.urlopen(url) as response:
        html = response.read().decode('utf-8', 'replace')
        ids = youtube_url_pat_re.findall(html)
        return set(ids)


class YouTubeInsertFailed(Exception):
    pass


url_stop_patterns = {
    'www.aweber.com',
    'oscar-peterson/ray-charles-shines-a-light',
    'jazzonthetube.com/content',
    'jazzonthetube.com/birthdays',
    'jazzonthetube.com/donate'
}
url_stop_patterns_re = re.compile('(?:%s)' % '|'.join(map(re.escape, url_stop_patterns)))


def main():
    my_credentials = credentials()
    http = my_credentials.authorize(httplib2.Http())

    youtube = Youtube(http)
    gmail = Gmail(http)

    for _id in reversed(gmail.query('from:"jazz on the tube" in:inbox')):
        msg = Message(_id, gmail)
        playlist = Playlist.by_date(msg.date, youtube)
        print('msg_id: %s, playlist: "%s", urls: %s' % (msg.id, playlist, msg.urls))
        for url in msg.urls:
            if not url_stop_patterns_re.search(url):
                for vid in video_ids_from_url(url):
                    try:
                        playlist.add(vid)
                    except YouTubeInsertFailed as e:
                        print(
                            'YouTube insert failed inserting %s (from msg %s) into %s: %s' % (vid, msg.id, playlist, e))
                        break
                else:
                    msg.archive()


if __name__ == '__main__':
    main()
